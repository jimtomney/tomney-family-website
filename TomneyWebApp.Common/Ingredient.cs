﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TomneyWebApp.Common
{
    public class Ingredient
    {
        public string Name { get; set; }
        public string Amount { get; set; }
        public string Note { get; set; }
    }
}
