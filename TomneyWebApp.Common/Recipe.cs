﻿using System.Collections.Generic;
using TomneyWebApp.Common.Interfaces;

namespace TomneyWebApp.Common
{
    public class Recipe : IRecipe
    {

        public string Description { get; set; }
        public int Id { get; set; }
        public Ratings Rating { get; set; }
        public List<RecipeStep> Steps { get; set; }
        public string Title { get; set; }
        public string Ingredients { get; set; }
        public byte[] Pdf { get; set; }
    }
}
