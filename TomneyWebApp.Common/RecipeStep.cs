﻿using System.Collections.Generic;

namespace TomneyWebApp.Common
{
    public class RecipeStep
    {
        public List<Ingredient> Ingredients { get; set; }
        public string Notes { get; set; }
    }
}
