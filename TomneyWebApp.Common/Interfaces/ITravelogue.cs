﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TomneyWebApp.Common.Interfaces
{
    public interface ITravelogue
    {
        int TravelogueId { get; set; }
        string Description { get; set; }
        string Title { get; set; }
        string Destination { get; set; }
        string ImgBase64 { get; set; }
    }
}
