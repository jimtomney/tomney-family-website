﻿using System.Collections.Generic;

namespace TomneyWebApp.Common.Interfaces
{
    public interface IRecipe
    {
        string Description { get; set; }
        int Id { get; set; }
        Ratings Rating { get; set; }
        string Ingredients { get; set; }
        List<RecipeStep> Steps { get; set; }
        string Title { get; set; }

        byte[] Pdf { get; set; }
    }
}