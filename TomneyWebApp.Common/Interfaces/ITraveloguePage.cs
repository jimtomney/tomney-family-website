﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TomneyWebApp.Common.Interfaces
{
    public interface ITraveloguePage
    {
        int PgNum { get; set; }
        string Parent { get; set; }
        string Html { get; set; }
        int NextPg { get; set; }
        int PriorPg { get; set; }
    }
}
