﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TomneyWebApp.Common.Interfaces.DataSrvc
{
    public interface ITravelogueData
    {
        IEnumerable<ITravelogue> GetAllTravelogues();

        TraveloguePage GetTraveloguePage(string name, int page);
    }
}
