﻿using System.Collections.Generic;

namespace TomneyWebApp.Common.Interfaces.DataSrvc
{
    public interface IRecipeData
    {
        //IEnumerable<IRecipe> GetAllRecipes();
        IEnumerable<IRecipe> GetRecipesByName(string name = null);

    }
}
