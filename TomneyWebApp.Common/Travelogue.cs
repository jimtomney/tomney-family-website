﻿using System;
using System.Collections.Generic;
using System.Text;
using TomneyWebApp.Common.Interfaces;

namespace TomneyWebApp.Common
{
    public class Travelogue : ITravelogue
    {
        public int TravelogueId { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public string Destination { get; set; }

        public string ImgBase64 { get; set; }
    }
}
