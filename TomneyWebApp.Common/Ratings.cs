﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TomneyWebApp.Common
{
    public enum Ratings
    {
        Excellent,
        Good,
        Fair,
        Poor
    }
}
