﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TomneyWebApp.Common
{
    public class LinkInfo
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public byte[] Thumbnail { get; set; }
        public bool HasThumbnail { get; set; }
        public string Url { get; set; }
    }
}
