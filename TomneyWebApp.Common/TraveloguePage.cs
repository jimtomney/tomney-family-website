﻿using System;
using System.Collections.Generic;
using System.Text;
using TomneyWebApp.Common.Interfaces;

namespace TomneyWebApp.Common
{
    public class TraveloguePage : ITraveloguePage
    {
        public int PgNum { get; set; }
        public string Parent { get; set; }
        public string Html { get; set; }
        public int NextPg { get; set; }
        public int PriorPg { get; set; }
    }
}
