﻿using System;
using System.Collections.Generic;
using System.Linq;
using MySql.Data.MySqlClient;
using TomneyWebApp.Common;
using TomneyWebApp.Common.Interfaces;
using TomneyWebApp.Common.Interfaces.DataSrvc;

namespace TomneyWebApp.MySqlDataSrvc
{
    public class MySqlRecipeDataSrvc : IRecipeData
    {
        private readonly string _cnnStr;

        public MySqlRecipeDataSrvc(string cnnStr)
        {
            _cnnStr = cnnStr;
        }

        public IEnumerable<IRecipe> GetRecipesByName(string name = null)
        {
            List<Recipe> list = new List<Recipe>();
            using (MySqlConnection conn = new MySqlConnection(_cnnStr))
            {
                string sql = (name == null) ? "SELECT * FROM mysql_50230_dev.recipe;" : $"SELECT * FROM mysql_50230_dev.recipe WHERE title = '{name}';";
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(sql, conn);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Recipe()
                        {
                            Description = (string) reader["description"],
                            Title = (string) reader["title"],
                            Id = (int) reader["id"],
                            Ingredients = (string) reader["ingredients"],
                            Pdf = (byte[])reader["pdf"]
                        });
                    }
                }
            }

            list = list.OrderBy(r => r.Title).ToList();
            return list;
        }
    }
}
