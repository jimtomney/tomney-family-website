﻿using System.Collections.Generic;
using MySql.Data.MySqlClient;
using TomneyWebApp.Common;
using TomneyWebApp.Common.Interfaces;
using TomneyWebApp.Common.Interfaces.DataSrvc;

namespace TomneyWebApp.MySqlDataSrvc
{
    public class MySqlTravelogueDataSrvc : ITravelogueData
    {
        private readonly string _cnnStr;

        public MySqlTravelogueDataSrvc(string cnnStr)
        {
            _cnnStr = cnnStr;
        }

        public IEnumerable<ITravelogue> GetAllTravelogues()
        {
            List<Travelogue> list = new List<Travelogue>();
            using (MySqlConnection conn = new MySqlConnection(_cnnStr))
            {
                string sql = "SELECT * FROM mysql_50230_dev.travelogue;";
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(sql, conn);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Travelogue()
                        {
                            Description = (string)reader["description"],
                            Destination = (string)reader["destination"],
                            ImgBase64 = (string)reader["img_base64"],
                            TravelogueId = (int)reader["travelogue_id"],
                            Title = (string)reader["title"]
                        });
                    }
                }
            }

            return list;
        }

        public TraveloguePage GetTraveloguePage(string name, int page)
        {
            TraveloguePage result = new TraveloguePage();
            using (MySqlConnection conn = new MySqlConnection(_cnnStr))
            {
                string sql = $"SELECT * FROM mysql_50230_dev.travelogue_page WHERE parent = '{name}' AND pg = {page};";
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(sql, conn);

                using var reader = cmd.ExecuteReader();
                reader.Read();
                result.Html = (string)reader["html"];
                result.NextPg = (int)reader["next_pg"];
                result.Parent = (string)reader["parent"];
                result.PgNum = (int)reader["pg"];
                result.PriorPg = (int)reader["prior_pg"];
            }   

            return result;
        }
    }
}
