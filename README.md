# README #

This is the code for tomney.com, our family website. Started in 2004 it has gone through various iterations, starting as static HTML pages through today where I'm porting it to ASP.NET Core 3.1 with some Azure functionality as a continuing education exercise.

## Release Notes ##

* Release 1.0.21037.1210
    * Initial release of .NET Core 3.1 MVC responsive site with Bootstrap
    * Implements login via Azure B2C AD with Google as an identity provider
    * Uses Serilog for logging to Azure blob
    * Azure storage serves as reference for images in Travelogue section
    * Uses MySql as backend server for serving some content (Travelogue pages and recipe PDF files)