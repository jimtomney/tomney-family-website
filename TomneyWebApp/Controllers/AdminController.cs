﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace TomneyWebApp.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        public IActionResult Default()
        {
            return View();
        }

        public IActionResult ErrorTest()
        {
            string x = "ABC";
            string y = x.Substring(4, 1);
            return View();
        }
    }
}
