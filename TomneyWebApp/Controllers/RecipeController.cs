﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TomneyWebApp.Common.Interfaces;
using TomneyWebApp.Common.Interfaces.DataSrvc;

namespace TomneyWebApp.Controllers
{
    public class RecipeController : Controller
    {
        private IRecipeData _dataSrv;

        public RecipeController(IRecipeData dataSrc, ILogger<RecipeController> logger)
        {
            _dataSrv = dataSrc;
            logger.LogWarning("MyWarning");
        }

        // GET: RecipeController
        public ActionResult Default()
        {
            List<IRecipe> lst = _dataSrv.GetRecipesByName().ToList();
            ViewData["Recipes"] = lst;
            return View();
        }

        // GET: RecipeController/Details/5
        public FileStreamResult Details(string title)
        {
            IRecipe recipe = _dataSrv.GetRecipesByName(title).FirstOrDefault();
            var stream = new MemoryStream(recipe.Pdf);
            return new FileStreamResult(stream, "application/pdf");
        }

        //// GET: RecipeController/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        //// POST: RecipeController/Create
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create(IFormCollection collection)
        //{
        //    try
        //    {
        //        return RedirectToAction(nameof(Index));
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //// GET: RecipeController/Edit/5
        //public ActionResult Edit(int id)
        //{
        //    return View();
        //}

        //// POST: RecipeController/Edit/5
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit(int id, IFormCollection collection)
        //{
        //    try
        //    {
        //        return RedirectToAction(nameof(Index));
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //// GET: RecipeController/Delete/5
        //public ActionResult Delete(int id)
        //{
        //    return View();
        //}

        //// POST: RecipeController/Delete/5
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Delete(int id, IFormCollection collection)
        //{
        //    try
        //    {
        //        return RedirectToAction(nameof(Index));
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}
    }
}
