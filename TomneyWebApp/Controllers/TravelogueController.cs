﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TomneyWebApp.Common;
using TomneyWebApp.Common.Interfaces;
using TomneyWebApp.Common.Interfaces.DataSrvc;
using TomneyWebApp.Models;

namespace TomneyWebApp.Controllers
{
    public class TravelogueController : Controller
    {
        private readonly ITravelogueData _dataSrvc;

        public TravelogueController(ITravelogueData dataSrvc)
        {
            _dataSrvc = dataSrvc;
        }

        // GET: TravelogueController
        public ActionResult Default()
        {
            List<ITravelogue> lst = _dataSrvc.GetAllTravelogues().ToList();
            ViewData["Travelogues"] = lst;
            return View();
        }

        public ActionResult Details(int id, string title)
        {
            TraveloguePage page = _dataSrvc.GetTraveloguePage(title, id);
            TraveloguePageVwMdl vm = new TraveloguePageVwMdl()
            {
                PriorPg = page.PriorPg,
                Parent = page.Parent,
                PgNum = page.PgNum,
                NextPg = page.NextPg,
                Html = page.Html
            };
            return View(vm);
        }

        //    // GET: TravelogueController/Create
        //    public ActionResult Create()
        //    {
        //        return View();
        //    }

        //    // POST: TravelogueController/Create
        //    [HttpPost]
        //    [ValidateAntiForgeryToken]
        //    public ActionResult Create(IFormCollection collection)
        //    {
        //        try
        //        {
        //            return RedirectToAction(nameof(Index));
        //        }
        //        catch
        //        {
        //            return View();
        //        }
        //    }

        //    // GET: TravelogueController/Edit/5
        //    public ActionResult Edit(int id)
        //    {
        //        return View();
        //    }

        //    // POST: TravelogueController/Edit/5
        //    [HttpPost]
        //    [ValidateAntiForgeryToken]
        //    public ActionResult Edit(int id, IFormCollection collection)
        //    {
        //        try
        //        {
        //            return RedirectToAction(nameof(Index));
        //        }
        //        catch
        //        {
        //            return View();
        //        }
        //    }

        //    // GET: TravelogueController/Delete/5
        //    public ActionResult Delete(int id)
        //    {
        //        return View();
        //    }

        //    // POST: TravelogueController/Delete/5
        //    [HttpPost]
        //    [ValidateAntiForgeryToken]
        //    public ActionResult Delete(int id, IFormCollection collection)
        //    {
        //        try
        //        {
        //            return RedirectToAction(nameof(Index));
        //        }
        //        catch
        //        {
        //            return View();
        //        }
        //    }
    }
}
