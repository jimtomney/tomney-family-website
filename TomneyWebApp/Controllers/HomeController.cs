﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using TomneyWebApp.Models;

namespace TomneyWebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IConfiguration _config;

        public HomeController(ILogger<HomeController> logger, IConfiguration config)
        {
            _logger = logger;
            _config = config;
        }

        public IActionResult Index()
        {
            var userId = this.User.FindFirstValue(ClaimTypes.NameIdentifier);
            return View();
        }

        public IActionResult Default()
        {

            bool isFamily = false;
            Claim userId = User.Claims.FirstOrDefault(c => c.Type == "emails");
            if (userId != null)
            {
                _logger.LogInformation("Email claim for logged in user is {userId}", userId);
                if (userId.Value.ToLower().Contains("tomney"))
                {
                    isFamily = true;
                }
            }
            ViewBag.Secret = _config["mysql-staging"];
            ViewBag.IsFamily = isFamily;
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
