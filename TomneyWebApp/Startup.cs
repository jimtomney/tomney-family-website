using System;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.AzureADB2C.UI;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using TomneyWebApp.Common.Interfaces.DataSrvc;
using TomneyWebApp.MySqlDataSrvc;

namespace TomneyWebApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            try
            {
                bool isDev = false;
                try
                {
                    isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT").ToUpper() == "DEVELOPMENT";
                }
                catch (Exception)
                {
                    //fall thru
                }
                

                services.AddAuthentication(AzureADB2CDefaults.AuthenticationScheme)
                    .AddAzureADB2C(options => Configuration.Bind("AzureAdB2C", options));
                services.AddControllersWithViews();

                string mySqlCnnStr = Configuration["ConnectionStrings:MySql"];

                services.Configure<CookiePolicyOptions>(options =>
                {
                    // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                    options.CheckConsentNeeded = context => true;
                    // requires using Microsoft.AspNetCore.Http;
                    options.MinimumSameSitePolicy = SameSiteMode.None;
                });
                services.AddSingleton<IRecipeData>(s => new MySqlRecipeDataSrvc(mySqlCnnStr));
                services.AddSingleton<ITravelogueData>(s => new MySqlTravelogueDataSrvc(mySqlCnnStr));
                services.AddRazorPages();
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Failure in Configuration method");
            }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "areaRoute",
                    pattern: "{area:exists}/{controller=Home}/{action=Default}/{id?}");
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Default}/{id?}");
                endpoints.MapRazorPages();
            });
        }
    }
}
