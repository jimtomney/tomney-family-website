﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TomneyWebApp.Common.Interfaces;

namespace TomneyWebApp.Models
{
    public class RecipeVwMdl
    {
        public List<IRecipe> Recipes { get; set; }
    }
}
