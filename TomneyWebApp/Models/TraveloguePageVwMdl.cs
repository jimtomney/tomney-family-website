﻿using TomneyWebApp.Common.Interfaces;

namespace TomneyWebApp.Models
{
    public class TraveloguePageVwMdl : ITraveloguePage
    {
        public int PgNum { get; set; }
        public string Parent { get; set; }
        public string Html { get; set; }
        public int NextPg { get; set; }
        public int PriorPg { get; set; }
    }
}
