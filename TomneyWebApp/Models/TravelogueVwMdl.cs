﻿using System.Collections.Generic;
using TomneyWebApp.Common.Interfaces;

namespace TomneyWebApp.Models
{
    public class TravelogueVwMdl
    {
        public List<ITravelogue> Travelogues { get; set; }
    }
}
