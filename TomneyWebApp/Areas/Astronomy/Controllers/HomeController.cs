﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace TomneyWebApp.Areas.Astronomy.Controllers
{
    [Area("Astronomy")]
    public class HomeController : Controller
    {
        public IActionResult Default()
        {
            return View();
        }

        public IActionResult AstroBio()
        {
            return View();
        }
    }
}
